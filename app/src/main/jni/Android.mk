# Copyright (C) 2009 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
#
LOCAL_PATH := $(call my-dir)
# dex的lib库，具有dex文件结构定义
LIB_DEX_PATH := $(LOCAL_PATH)/libdex
# dalvik内部的原始原始数据类型定义，我们做hack的时候需要用到这些数据结构,这些函数很多都调用了dalvik内部apk，所以我们通过hack的方式来路由
LIB_DVM_OO_PATH := $(LOCAL_PATH)/oo
#zlib
ZLIB_SRC_PATH := $(LOCAL_PATH)/zlib

include $(CLEAR_VARS)
# LOCAL_CPP_FEATURES += exceptions
LOCAL_MODULE    := unshellnative
LOCAL_SRC_FILES := inlineHook.c relocate.c dlopen.c shell_version1.cpp  init.cpp NativeStackTrace.cpp socrack.cpp  \
             	$(LIB_DEX_PATH)/CmdUtils.cpp \
             	$(LIB_DEX_PATH)/DexCatch.cpp \
             	$(LIB_DEX_PATH)/DexClass.cpp \
             	$(LIB_DEX_PATH)/DexDataMap.cpp \
             	$(LIB_DEX_PATH)/DexDebugInfo.cpp \
             	$(LIB_DEX_PATH)/DexFile.cpp \
             	$(LIB_DEX_PATH)/DexInlines.cpp \
             	$(LIB_DEX_PATH)/DexOptData.cpp \
             	$(LIB_DEX_PATH)/DexOpcodes.cpp \
             	$(LIB_DEX_PATH)/DexProto.cpp \
             	$(LIB_DEX_PATH)/DexSwapVerify.cpp \
             	$(LIB_DEX_PATH)/DexUtf.cpp \
             	$(LIB_DEX_PATH)/InstrUtils.cpp \
             	$(LIB_DEX_PATH)/Leb128.cpp \
             	$(LIB_DEX_PATH)/OptInvocation.cpp \
             	$(LIB_DEX_PATH)/sha1.cpp \
             	$(LIB_DEX_PATH)/SysUtil.cpp \
             	$(LIB_DEX_PATH)/ZipArchive.cpp \
             	$(LIB_DEX_PATH)/safe_iop.c\
             	$(LIB_DVM_OO_PATH)/AccessCheck.cpp\
             	$(LIB_DVM_OO_PATH)/Array.cpp\
             	$(LIB_DVM_OO_PATH)/Class.cpp\
             	$(LIB_DVM_OO_PATH)/Object.cpp\
             	$(LIB_DVM_OO_PATH)/Resolve.cpp\
             	$(LIB_DVM_OO_PATH)/TypeCheck.cpp\
             	$(ZLIB_SRC_PATH)/adler32.c \
             	$(ZLIB_SRC_PATH)/compress.c \
             	$(ZLIB_SRC_PATH)/crc32.c \
             	$(ZLIB_SRC_PATH)/deflate.c \
             	$(ZLIB_SRC_PATH)/gzclose.c \
             	$(ZLIB_SRC_PATH)/gzlib.c \
             	$(ZLIB_SRC_PATH)/gzread.c \
             	$(ZLIB_SRC_PATH)/gzwrite.c \
             	$(ZLIB_SRC_PATH)/infback.c \
             	$(ZLIB_SRC_PATH)/inflate.c \
             	$(ZLIB_SRC_PATH)/inftrees.c \
             	$(ZLIB_SRC_PATH)/inffast.c \
             	$(ZLIB_SRC_PATH)/trees.c \
             	$(ZLIB_SRC_PATH)/uncompr.c \
             	$(ZLIB_SRC_PATH)/zutil.c



LOCAL_LDLIBS := -llog
LOCAL_CFLAGS += -funwind-tables -g
include $(BUILD_SHARED_LIBRARY)
